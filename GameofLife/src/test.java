import static org.junit.Assert.*;

import org.junit.Test;


public class test {

	@Test
	public void test1() {
		liveStatus expected = new liveStatus(false,0);
		liveStatus output = new liveStatus(true,1);
		assertEquals(GameLife.liveordie(output),expected.isalive);

	}

	@Test
	public void test2() {
		liveStatus expected = new liveStatus(true,0);
		liveStatus output = new liveStatus(true,2);
		assertEquals(GameLife.liveordie(output),expected.isalive);
	}
	
	@Test
	public void test3() {
		liveStatus expected = new liveStatus(false,0);
		liveStatus output = new liveStatus(true,4);
		assertEquals(GameLife.liveordie(output),expected.isalive);
	}

	@Test
	public void test4() {
		liveStatus expected = new liveStatus(true,0);
		liveStatus output = new liveStatus(false,3);
		assertEquals(GameLife.liveordie(output),expected.isalive);
	}
	
	@Test
	public void test4_2() {
		liveStatus expected = new liveStatus(false,0);
		liveStatus output = new liveStatus(true,5);
		assertEquals(GameLife.liveordie(output),expected.isalive);
	}
	
	@Test
	public void test4_3() {
		liveStatus expected = new liveStatus(false,0);
		liveStatus output = new liveStatus(false,0);
		assertEquals(GameLife.liveordie(output),expected.isalive);
	}


}


