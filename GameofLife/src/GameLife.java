public class GameLife {
//Coderetreat pair programming Feb 26, 2016.
	
	public static boolean liveordie(liveStatus cell) {
		if (cell.isalive == true) {
			if (cell.numberoflivingneighbors < 2
					|| cell.numberoflivingneighbors > 3) {
				cell.isalive = false;

			} else {
				cell.isalive = true;
			}
		} else if (cell.numberoflivingneighbors == 3)
			cell.isalive = true;

		return cell.isalive;

	}

}
